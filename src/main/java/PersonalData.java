public class PersonalData {
    private Name givenNames;
    private Name familyNames;
    private int age;


    public PersonalData() {
    }

    public PersonalData(Name givenNames, Name familyNames, int age) {
        this.givenNames = givenNames;
        this.familyNames = familyNames;
        this.age = age;
    }


    public Name getGivenname() {
        return givenNames;
    }

    public void setGivenname(Name givenNames) {
        this.givenNames = givenNames;
    }

    public Name getFamilyname() {
        return familyNames;
    }

    public void setFamilyname(Name familyNames) {
        this.familyNames = familyNames; 
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
