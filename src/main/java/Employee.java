public class Employee {
    private int id;
    private PersonalData personalData;

    public Employee() {
    }

    public Employee(int id, PersonalData personalData) {
        this.id = id;
        this.personalData = personalData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PersonalData getPersonaldata() {
        return personalData;
    }

    public void setPersonaldata(PersonalData personaldata) {
        this.personalData = personaldata;
    }
}


