import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

public class XMLConverter {
    public static void main(String[] args) throws JAXBException, JsonProcessingException {
        Employee employee = xmlToPOJO();
        POJOtoJSON(employee);
    }

    private static Employee xmlToPOJO() throws JAXBException {
        StreamSource xml = new StreamSource("employee.xml");

        JAXBContext jaxbContext = JAXBContext.newInstance(Employee.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        JAXBElement<Employee> jaxbElem = unmarshaller.unmarshal(xml, Employee.class);

        return (jaxbElem.getValue());
    }

    private static void POJOtoJSON(Employee employee) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);

        String json = mapper.writeValueAsString(employee);
        System.out.println(json);
    }

}
